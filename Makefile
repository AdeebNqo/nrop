c = g++
options = -L/usr/include/ -lboost_program_options
app_name = nrop
source = main.cpp
$(app_name): $(source)
	@$(c) -o $(app_name) main.cpp $(options)
clean:
	@rm -f $(app_name)
run:
	@./$(app_name)
