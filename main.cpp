#include<boost/program_options.hpp>
int main(int argc, char* argv[]){
	/*
	taking in the options
	*/
	namespace po = boost::program_options;
	po::options_description desc("Allowed options");
	desc.add_options()
     	("encrypt,e", "encrypt file")
	("decrypt,d","decrypt file")
	("key,k",po::value<int>(),"key of the cipher")
	("file,f",po::value<std::string>(),"file to be encrypted or decrypted");

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);
	/*
	evaluating the options
	*/
	bool encrypt = false;
	bool decrypt = false;
	std::string filename="";	
	int key = 1234;

	if (vm.count("decrypt")>0){
		decrypt = true;
	}
	if (vm.count("encrypt")>0){
		encrypt = true;
	}
	filename = vm["file"].as<std::string>();
	key = vm["key"].as<int>();
	/*

	applying the cipher
	
	*/
	std::ofstream output(filename);
	std::ifstream input(filename);
		
	std::istream_iterator<char> in_curr(input);
	std::istream_iterator<char> end;

	std::ostream_iterator<char> out_curr(output,"");
	
	//decrypting option
	if (decrypt){
		std::transform(in_curr, end, out_curr, 
			
		);
	}
	else if (encrypt){
		std::transform(in_curr, end, out_curr,

		);
	}
	return 0;
}
